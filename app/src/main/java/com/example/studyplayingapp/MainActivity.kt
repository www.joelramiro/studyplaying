package com.example.studyplayingapp

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun PlayEvent(view: View)
    {
        Toast.makeText(this,"play",Toast.LENGTH_LONG).show()
    }

    fun ExitEvent(view: View)
    {
        Toast.makeText(this,"exit",Toast.LENGTH_LONG).show()
        finish()
    }
}